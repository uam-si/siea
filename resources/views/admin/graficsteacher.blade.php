@extends('layouts.app')

@section('content')

    <?php
//    foreach ($grafics as $key => $grafic) {
//
//        dump($key, $grafic);
//   }
//    dd(1);
    ?>

    <div class="row">
        <div class="col-md-12">
            <h2 style="margin-top: 0;">{{ $graficTitle }}</h2>

        <!-- Area Chart -->
{{--        <div class="col-xl-8 col-lg-7">--}}
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">{{$graficLabel}}</h6>
                    <div class="dropdown no-arrow">
                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                        </a>
                            <div class="dropdown-menu dropdown-menu-right shadow animated&#45;&#45;fade-in" aria-labelledby="dropdownMenuLink">
                              <div class="dropdown-header">Dropdown:</div>
                              <a class="dropdown-item" href="#">Download</a>
                              <div class="dropdown-divider"></div>
                              <a class="dropdown-item" href="#">Detalhar resultados</a>
                            </div>
                    </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <div class="chart-area">
                        <canvas id="myChart{{ $graficId }}"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>

            <script>

                new Chart(document.getElementById("myChart{{ $graficId }}"), {
                    type: '{{ $chartType }}',
                    data: {

                        labels: [
                            @foreach ($grafics as $result)
                                "{{ $result->turno }}",
                            @endforeach
                        ],

                        datasets: [{
                            data: [86,114,106,106,107,111,133,221,783,2478],
                            label: 'TURMA CCOMP2N02A',
                            borderColor: "#3e95cd",
                            fill: false
                        },
                            {
                            data: [282,350,411,502,635,809,947,1402,3700,5267],
                            label: "TURMA CCOMP2N03A",
                            borderColor: "#8e5ea2",
                            fill: false
                        }, {
                            data: [168,170,178,190,203,276,408,547,675,734],
                            label: "TURMA CCOMP2N04A",
                            borderColor: "#3cba9f",
                            fill: false
                        }, {
                            data: [40,20,10,16,24,38,74,167,508,784],
                            label: "TURMA CCOMP2N05A",
                            borderColor: "#e8c3b9",
                            fill: false
                        }, {
                            data: [6,3,2,2,7,26,82,172,312,433],
                            label: "TURMA CCOMP2N08A",
                            borderColor: "#c45850",
                            fill: false
                        }
                        ]
                    },
                    options: {
                        title: {
                            display: true,
                            text: 'Notas Por Turma'
                        }
                    }
                });
            </script>
@stop