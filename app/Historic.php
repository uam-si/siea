<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;


class Historic extends Model
{
    use SoftDeletes;

    protected $fillable = ['matricula', 'nome', 'unidade', 'turno', 'turma', 'gabarito', 'conhecimentos_especificos',
        'acertos_conhecimentos_especificos', 'porcentagem_conhecimentos_especificos', 'fundamentacao_geral', 'acertos_fundamentacao_geral',
        'porcentagem_fundamentacao_geral', 'nota_do_teste', 'total_de_acertos', 'total_porcentagem', 'class_geral', 'class_unidade', 'class_turma', 'stats_date'];
    protected $hidden = [];

    /**
     * Set attribute to date format
     * @param $input
     */
    public function setStatsDateAttribute($input)
    {
        if ($input != null && $input != '') {
            $this->attributes['stats_date'] = Carbon::createFromFormat(config('app.date_format'), $input)->format('Y-m-d');
        } else {
            $this->attributes['stats_date'] = null;
        }
    }

    /**
     * Get attribute from date format
     * @param $input
     *
     * @return string
     */
    public function getStatsDateAttribute($input)
    {
        $zeroDate = str_replace(['Y', 'm', 'd'], ['0000', '00', '00'], config('app.date_format'));
        if ($input != $zeroDate && $input != null) {
            return Carbon::createFromFormat('Y-m-d', $input)->format(config('app.date_format'));
        } else {
            return '';
        }
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setMatriculaAttribute($input)
    {
        $this->attributes['matricula'] = $input ? $input : null;
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setNomesAttribute($input)
    {
        $this->attributes['nome'] = $input ? $input : null;
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setUnidadesAttribute($input)
    {
        $this->attributes['unidade'] = $input ? $input : null;
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setTurnoAttribute($input)
    {
        $this->attributes['turno'] = $input ? $input : null;
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setTurmaAttribute($input)
    {
        $this->attributes['turma'] = $input ? $input : null;
    }

}