<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Engagement;
use Carbon\Carbon;


class ReportsController extends Controller
{
    public function fans()
    {
        $reportTitle = 'Notas de turmas';
        $reportLabel = 'SUM';
        $chartType   = 'line';

        $reports = Engagement::get()->sortBy('stats_date')->groupBy(function ($entry) {

            if ($entry->stats_date instanceof \Carbon\Carbon) {
                return \Carbon\Carbon::parse($entry->stats_date)->format('Y-m-d');
            }

            return \Carbon\Carbon::createFromFormat(config('app.date_format'), $entry->stats_date)->format('Y-m-d');
        })->map(function ($entries, $group) {
            //dd($entries->toArray());
            return $entries->sum('fans');
        });

        return view('admin.reports', compact('reportTitle','reports', 'chartType', 'reportLabel'));
    }

    public function engagements()
    {
        $reportTitle = 'Comparar resultados';
        $reportLabel = 'SUM';
        $chartType   = 'bar';

        $reports = Engagement::get()->sortBy('stats_date')->groupBy(function ($entry) {

            if ($entry->stats_date instanceof \Carbon\Carbon) {
                return \Carbon\Carbon::parse($entry->stats_date)->format('Y-m-d');
            }

            return \Carbon\Carbon::createFromFormat(config('app.date_format'), $entry->stats_date)->format('Y-m-d');
        })->map(function ($entries, $group) {
            return $entries->sum('engagements');
        });

        return view('admin.reports', compact('reportTitle', 'reports', 'chartType', 'reportLabel'));
    }

    public function reactions()
    {
        $reportTitle = 'Classificação alunos';
        $reportLabel = 'SUM';
        $chartType   = 'line';

        $reports = Engagement::get()->sortBy('stats_date')->groupBy(function ($entry) {
            if ($entry->stats_date instanceof \Carbon\Carbon) {
                return \Carbon\Carbon::parse($entry->stats_date)->format('Y-m-d');
            }

            return \Carbon\Carbon::createFromFormat(config('app.date_format'), $entry->stats_date)->format('Y-m-d');
        })->map(function ($entries, $group) {
            return $entries->sum('reactions');
        });

        return view('admin.reports', compact('reportTitle', 'reports', 'chartType', 'reportLabel'));
    }

    public function comments()
    {
        $reportTitle = 'Comparativos do semestre';
        $reportLabel = 'SUM';
        $chartType   = 'line';

        $reports = Engagement::get()->sortBy('stats_date')->groupBy(function ($entry) {
            if ($entry->stats_date instanceof \Carbon\Carbon) {
                return \Carbon\Carbon::parse($entry->stats_date)->format('Y-m-d');
            }

            return \Carbon\Carbon::createFromFormat(config('app.date_format'), $entry->stats_date)->format('Y-m-d');
        })->map(function ($entries, $group) {
            return $entries->sum('comments');
        });

        return view('admin.reports', compact('reportTitle', 'reports', 'chartType', 'reportLabel'));
    }

    public function shares()
    {
        $reportTitle = 'Shares';
        $reportLabel = 'SUM';
        $chartType   = 'line';

        $reports = Engagement::get()->sortBy('stats_date')->groupBy(function ($entry) {
            if ($entry->stats_date instanceof \Carbon\Carbon) {
                return \Carbon\Carbon::parse($entry->stats_date)->format('Y-m-d');
            }

            return \Carbon\Carbon::createFromFormat(config('app.date_format'), $entry->stats_date)->format('Y-m-d');
        })->map(function ($entries, $group) {
            return $entries->sum('shares');
        });

        return view('admin.reports', compact('reportTitle', 'reports', 'chartType', 'reportLabel'));
    }

}
