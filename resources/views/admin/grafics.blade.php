@extends('layouts.app')

@section('content')
<?php
//foreach ($grafics as $grafic) {
//    var_dump($grafic);
//}

//foreach ($grafics2 as $grafic2) {
//    var_dump($grafic2);
//}
?>
    <div class="row">
        {{--        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>--}}
        <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>

        <div class="col-md-12">
            <h2 style="margin-top: 0;">{{ $graficTitle }}</h2>

        <!-- Area Chart -->
{{--        <div class="col-xl-8 col-lg-7">--}}
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Semestre 7º - Prova realizada em 17 de maio de 2019</h6>
                    <div class="dropdown no-arrow">
                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                        </a>
                            <div class="dropdown-menu dropdown-menu-right shadow animated&#45;&#45;fade-in" aria-labelledby="dropdownMenuLink">
                              <div class="dropdown-header">Dropdown Header:</div>
                              <a class="dropdown-item" href="#">Action</a>
                              <a class="dropdown-item" href="#">Another action</a>
                              <div class="dropdown-divider"></div>-->
                              <a class="dropdown-item" href="#">Something else here</a>
                            </div>
                    </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <div class="chart-area">
                        <canvas id="myAreaChart{{ $graficId }}"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        // Area Chart Example
        var ctx = document.getElementById("myAreaChart{{ $graficId }}");
        var myLineChart = new Chart(ctx, {
            type: '{{ $chartType }}',
            data: {
                labels: [
                    @foreach ($grafics as $group => $result)
                        "semestre {{ $group }} º",
                    @endforeach
                ],
                datasets: [{
                    label: '{{ $graficLabel }}',
                    lineTension: 0.3,
                    backgroundColor: "rgba(78, 115, 223, 0.05)",
                    borderColor: "rgba(78, 115, 223, 1)",
                    pointRadius: 3,
                    pointBackgroundColor: "rgba(78, 115, 223, 1)",
                    pointBorderColor: "rgba(78, 115, 223, 1)",
                    pointHoverRadius: 3,
                    pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
                    pointHoverBorderColor: "rgba(78, 115, 223, 1)",
                    pointHitRadius: 10,
                    pointBorderWidth: 2,
                    data: [
                        @foreach ($grafics as $result)
                        {!! $result !!},
                        @endforeach
                    ],
                }],

                labels: [
                    @foreach ($grafics as $group => $result)
                        "semestre {{ $group }} º",
                    @endforeach
                ],
                datasets: [{
                    label: '{{ $graficLabel }}',
                    lineTension: 0.3,
                    backgroundColor: "rgba(78, 115, 223, 0.05)",
                    borderColor: "rgba(78, 115, 223, 1)",
                    pointRadius: 3,
                    pointBackgroundColor: "rgba(78, 115, 223, 1)",
                    pointBorderColor: "rgba(78, 115, 223, 1)",
                    pointHoverRadius: 3,
                    pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
                    pointHoverBorderColor: "rgba(78, 115, 223, 1)",
                    pointHitRadius: 10,
                    pointBorderWidth: 2,
                    data: [
                        @foreach ($grafics as $group => $result)
                        {!! $result !!},
                        @endforeach
                    ],
                }],
            },
            options: {
                maintainAspectRatio: false,
                layout: {
                    padding: {
                        left: 10,
                        right: 25,
                        top: 25,
                        bottom: 0
                    }
                },
                scales: {
                    xAxes: [{
                        time: {
                            unit: 'date'
                        },
                        gridLines: {
                            display: false,
                            drawBorder: false
                        },
                        ticks: {
                            maxTicksLimit: 7
                        }
                    }],
                    yAxes: [{
                        ticks: {
                            maxTicksLimit: 5,
                            padding: 10,
                            // Include a dollar sign in the ticks
                            callback: function(value, index, values) {
                                return 'posicão ' + number_format(value);
                            }
                        },
                        gridLines: {
                            color: "rgb(234, 236, 244)",
                            zeroLineColor: "rgb(234, 236, 244)",
                            drawBorder: false,
                            borderDash: [2],
                            zeroLineBorderDash: [2]
                        }
                    }],
                },
                legend: {
                    display: false
                },
                tooltips: {
                    backgroundColor: "rgb(255,255,255)",
                    bodyFontColor: "#858796",
                    titleMarginBottom: 10,
                    titleFontColor: '#6e707e',
                    titleFontSize: 14,
                    borderColor: '#dddfeb',
                    borderWidth: 1,
                    xPadding: 15,
                    yPadding: 15,
                    displayColors: false,
                    intersect: false,
                    mode: 'index',
                    caretPadding: 10,
                    callbacks: {
                        label: function(tooltipItem, chart) {
                            var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
                            return datasetLabel + ': posicão ' + number_format(tooltipItem.yLabel);
                        }
                    }
                }
            }
        });


        Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
        Chart.defaults.global.defaultFontColor = '#858796';

        function number_format(number, decimals, dec_point, thousands_sep) {
            // *     example: number_format(1234.56, 2, ',', ' ');
            // *     return: '1 234,56'
            number = (number + '').replace(',', '').replace(' ', '');
            var n = !isFinite(+number) ? 0 : +number,
                prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
                sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
                dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
                s = '',
                toFixedFix = function(n, prec) {
                    var k = Math.pow(10, prec);
                    return '' + Math.round(n * k) / k;
                };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '').length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1).join('0');
            }
            return s.join(dec);
        }
    </script>





{{--            <canvas id="myChart{{ $graficTitle }}"></canvas>--}}
{{--            <script>--}}
{{--                var ctx = document.getElementById("myChart{{ $graficTitle }}").getContext('2d');--}}

{{--                var chart = new Chart(ctx, {--}}
{{--                    // The type of chart we want to create--}}
{{--                    type: '{{ $chartType }}',--}}

{{--                    // The data for our dataset--}}
{{--                    data: {--}}
{{--                        labels: [--}}
{{--                            @foreach ($grafics as $group => $result)--}}
{{--                                "{{ $group }}",--}}
{{--                            @endforeach--}}
{{--                        ],--}}
{{--                        datasets: [{--}}
{{--                            label: '{{ $graficLabel }}',--}}
{{--                            backgroundColor: 'rgb(255, 99, 132)',--}}
{{--                            borderColor: 'rgb(255, 99, 132)',--}}
{{--                            data: [--}}
{{--                                @foreach ($grafics as $group => $result)--}}
{{--                                {!! $result !!},--}}
{{--                                @endforeach--}}
{{--                            ]--}}
{{--                        }]--}}
{{--                    },--}}

{{--                    // Configuration options go here--}}
{{--                    options: {}--}}
{{--                });--}}
{{--            </script>--}}

{{--            <script>--}}
{{--                var ctx = document.getElementById("myChart{{ $graficTitle }}");--}}
{{--                var myChart = new Chart(ctx, {--}}
{{--                    type: '{{ $chartType }}',--}}
{{--                    data: {--}}
{{--                        labels: [--}}
{{--                            @foreach ($grafics as $group => $result)--}}
{{--                                "{{ $group }}",--}}
{{--                            @endforeach--}}
{{--                        ],--}}

{{--                        datasets: [{--}}
{{--                            label: '{{ $graficLabel }}',--}}
{{--                            data: [--}}
{{--                                @foreach ($grafics as $group => $result)--}}
{{--                                    {!! $result !!},--}}
{{--                                @endforeach--}}
{{--                            ],--}}
{{--                            borderWidth: 1--}}
{{--                        }]--}}
{{--                    },--}}
{{--                    options: {--}}
{{--                        scales: {--}}
{{--                            yAxes: [{--}}
{{--                                ticks: {--}}
{{--                                    beginAtZero:true--}}
{{--                                }--}}
{{--                            }]--}}
{{--                        }--}}
{{--                    }--}}
{{--                });--}}
{{--            </script>--}}

{{--        @endforeach--}}

@stop