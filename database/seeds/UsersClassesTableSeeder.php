<?php

use Illuminate\Database\Seeder;
use App\Classe;
use App\UserClasse;

class UsersClassesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Classe::create(['user_id' => 2, 'classe_id' => 1]);
        Classe::create(['user_id' => 2, 'classe_id' => 2]);
        Classe::create(['user_id' => 2, 'classe_id' => 3]);
    }
}
