@extends('layouts.auth')

@section('content')

    <!-- Outer Row -->
    <div class="row justify-content-center">

        <div class="col-xl-10 col-lg-12 col-md-9">

            <div id="header"><img src="https://auth.laureate.net/adfs/portal/logo/uam.png" alt="Logo Image"></div>
            <br/>
            <h3 class="h6 text-white">SIEA - SISTEMA DE INDICADORES E EVOLUÇÃO DO ALUNO</h3>
            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
                        <div class="col-lg-6">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">SEJA BEM VINDO!</h1>
                                </div>

                                                    @if (count($errors) > 0)
                                                        <div class="alert alert-danger">
                                                            <strong>@lang('global.app_whoops')</strong> @lang('global.app_there_were_problems_with_input'):
                                                            <br><br>
                                                            <ul>
                                                                @foreach ($errors->all() as $error)
                                                                    <li>{{ $error }}</li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    @endif

                                <form class="user"
                                          role="form"
                                          method="POST"
                                          action="{{ url('login') }}">
                                        <input type="hidden"
                                               name="_token"
                                               value="{{ csrf_token() }}">

{{--                                    <div class="form-group">--}}
{{--                                        <input type="email" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Entre com Email ou RA...">--}}
{{--                                    </div>--}}
{{--                                    <div class="form-group">--}}
{{--                                        <input type="password" class="form-control form-control-user" id="exampleInputPassword" placeholder="Senha">--}}
{{--                                    </div>--}}

                                    <div class="form-group">
{{--                                        <label class="col-md-4 control-label">@lang('global.app_email')</label>--}}


                                            <input type="email"
                                                   class="form-control form-control-user"
                                                   name="email"
                                                   value="{{ old('email') }}" placeholder="Entre com Email ou RA...">

                                    </div>

                                    <div class="form-group">
{{--                                        <label class="col-md-4 control-label">@lang('global.app_password')</label>--}}

                                            <input type="password"
                                                   class="form-control form-control-user"
                                                   name="password" placeholder="Senha">

                                    </div>


{{--                                    <div class="form-group">--}}
{{--                                        <div class="custom-control custom-checkbox small">--}}
{{--                                            <input type="checkbox" class="custom-control-input" id="customCheck">--}}
{{--                                            <label class="custom-control-label" for="customCheck">Lembre de mim</label>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <a href="index.html" class="btn btn-primary btn-user btn-block">--}}
{{--                                        ENTRAR--}}
{{--                                    </a>--}}
{{--                                    .--}}


                                    <div class="form-group">
                                        <div class="custom-control custom-checkbox small">
                                            <a href="{{ route('auth.password.reset') }}">@lang('global.app_forgot_password')</a>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                            <label>
                                                <input type="checkbox"
                                                       name="remember"> @lang('global.app_remember_me')
                                            </label>
                                    </div>

                                    <div class="form-group">

                                            <button type="submit"
                                                    class="btn btn-primary btn-user btn-block">
                                                @lang('global.app_login')
                                            </button>

                                    </div>

                                </form>

                                <hr>
{{--                                <div class="text-center">--}}
{{--                                    <a class="small" href="forgot-password.html">Esqueceu a senha?</a>--}}
{{--                                </div>--}}
                                <div class="text-center">
                                    <a class="small" href="#">Preciso de ajuda para fazer login</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>


{{--    <div class="row">--}}
{{--        <div class="col-md-8 col-md-offset-2">--}}
{{--            <div class="panel panel-default">--}}
{{--                <div class="panel-heading">{{ ucfirst(config('app.name')) }} @lang('global.app_login')</div>--}}
{{--                <div class="panel-body">--}}

{{--                    @if (count($errors) > 0)--}}
{{--                        <div class="alert alert-danger">--}}
{{--                            <strong>@lang('global.app_whoops')</strong> @lang('global.app_there_were_problems_with_input'):--}}
{{--                            <br><br>--}}
{{--                            <ul>--}}
{{--                                @foreach ($errors->all() as $error)--}}
{{--                                    <li>{{ $error }}</li>--}}
{{--                                @endforeach--}}
{{--                            </ul>--}}
{{--                        </div>--}}
{{--                    @endif--}}

{{--                    <form class="form-horizontal"--}}
{{--                          role="form"--}}
{{--                          method="POST"--}}
{{--                          action="{{ url('login') }}">--}}
{{--                        <input type="hidden"--}}
{{--                               name="_token"--}}
{{--                               value="{{ csrf_token() }}">--}}

{{--                        <div class="form-group">--}}
{{--                            <label class="col-md-4 control-label">@lang('global.app_email')</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input type="email"--}}
{{--                                       class="form-control"--}}
{{--                                       name="email"--}}
{{--                                       value="{{ old('email') }}">--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group">--}}
{{--                            <label class="col-md-4 control-label">@lang('global.app_password')</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input type="password"--}}
{{--                                       class="form-control"--}}
{{--                                       name="password">--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group">--}}
{{--                            <div class="col-md-6 col-md-offset-4">--}}
{{--                                <a href="{{ route('auth.password.reset') }}">@lang('global.app_forgot_password')</a>--}}
{{--                            </div>--}}
{{--                        </div>--}}


{{--                        <div class="form-group">--}}
{{--                            <div class="col-md-6 col-md-offset-4">--}}
{{--                                <label>--}}
{{--                                    <input type="checkbox"--}}
{{--                                           name="remember"> @lang('global.app_remember_me')--}}
{{--                                </label>--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group">--}}
{{--                            <div class="col-md-6 col-md-offset-4">--}}
{{--                                <button type="submit"--}}
{{--                                        class="btn btn-primary"--}}
{{--                                        style="margin-right: 15px;">--}}
{{--                                    @lang('global.app_login')--}}
{{--                                </button>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </form>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
@endsection