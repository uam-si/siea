<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoricsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historics', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->integer('matricula', 100)->unique();
            $table->string('nome');
            $table->string('unidade');
            $table->string('turno');
            $table->string('turma');
            $table->string('gabarito');
            $table->integer('conhecimentos_especificos');
            $table->integer('acertos_conhecimentos_especificos');
            $table->string('porcentagem_conhecimentos_especificos');
            $table->integer('fundamentacao_geral');
            $table->integer('acertos_fundamentacao_geral');
            $table->string('porcentagem_fundamentacao_geral');
            $table->double('nota_do_teste', 8, 2);
            $table->integer('total_de_acertos');
            $table->string('total_porcentagem');
            $table->integer('class_geral');
            $table->integer('class_unidade');
            $table->integer('class_turma');
            $table->date('stats_date')->nullable();
            $table->timestamps();
            $table->date('deleted_at')->nullable();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historics');
    }
}