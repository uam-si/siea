@inject('request', 'Illuminate\Http\Request')
<!-- Left side column. contains the sidebar -->

{{--<aside class="main-sidebar">--}}
{{--    <!-- sidebar: style can be found in sidebar.less -->--}}
{{--    <section class="sidebar">--}}
{{--        <ul class="sidebar-menu">--}}

{{--            <li class="{{ $request->segment(1) == 'home' ? 'active' : '' }}">--}}
{{--                <a href="{{ url('/') }}">--}}
{{--                    <i class="fa fa-wrench"></i>--}}
{{--                    <span class="title">@lang('global.app_dashboard')</span>--}}
{{--                </a>--}}
{{--            </li>--}}

{{--            @can('user_management_access')--}}
{{--            <li class="treeview">--}}
{{--                <a href="#">--}}
{{--                    <i class="fa fa-users"></i>--}}
{{--                    'Gerenciamento de usuários'--}}
{{--                    <span class="title">@lang('global.user-management.title')</span>--}}
{{--                    <span class="pull-right-container">--}}
{{--                        <i class="fa fa-angle-left pull-right"></i>--}}
{{--                    </span>--}}
{{--                </a>--}}
{{--                <ul class="treeview-menu">--}}

{{--                @can('permission_access')--}}
{{--                <li class="{{ $request->segment(2) == 'permissions' ? 'active active-sub' : '' }}">--}}
{{--                        <a href="{{ route('admin.permissions.index') }}">--}}
{{--                            <i class="fa fa-briefcase"></i>--}}
{{--                            <span class="title">--}}
{{--                                Permissões--}}
{{--                                @lang('global.permissions.title')--}}
{{--                            </span>--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                @endcan--}}
{{--                @can('role_access')--}}
{{--                <li class="{{ $request->segment(2) == 'roles' ? 'active active-sub' : '' }}">--}}
{{--                        <a href="{{ route('admin.roles.index') }}">--}}
{{--                            <i class="fa fa-briefcase"></i>--}}
{{--                            <span class="title">--}}
{{--                                Função--}}
{{--                                @lang('global.roles.title')--}}
{{--                            </span>--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                @endcan--}}
{{--                @can('user_access')--}}
{{--                <li class="{{ $request->segment(2) == 'users' ? 'active active-sub' : '' }}">--}}
{{--                        <a href="{{ route('admin.users.index') }}">--}}
{{--                            <i class="fa fa-user"></i>--}}
{{--                            <span class="title">--}}
{{--                                Usuários--}}
{{--                                @lang('global.users.title')--}}
{{--                            </span>--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                @endcan--}}
{{--                </ul>--}}
{{--            </li>--}}
{{--            @endcan--}}

{{--            @if(\Auth::user()->aluno ==0)--}}
{{--            @can('engagement_access')--}}
{{--            <li class="{{ $request->segment(2) == 'historics' ? 'active' : '' }}">--}}
{{--                Realizar Carga TP--}}
{{--                <a href="{{ route('admin.historics.index') }}">--}}
{{--                    <i class="fa fa-gears"></i>--}}
{{--                    <span class="title">@lang('global.historics.title')</span>--}}
{{--                </a>--}}
{{--            </li>--}}
{{--            @endcan--}}
{{--            @endif--}}


{{--            @if(\Auth::user()->aluno == 1)--}}
{{--            <li class="treeview">--}}
{{--                <a href="#">--}}
{{--                    <i class="fa fa-line-chart"></i>--}}
{{--                    <span class="title">Indicadores Aluno</span>--}}
{{--                    <span class="pull-right-container">--}}
{{--                        <i class="fa fa-angle-left pull-right"></i>--}}
{{--                    </span>--}}
{{--                </a>--}}
{{--                <ul class="treeview-menu">--}}
{{--                   <li class="{{ $request->is('/reports/fans') }}">--}}
{{--                        <a href="{{ url('/admin/reports/fans') }}">--}}
{{--                            <i class="fa fa-line-chart"></i>--}}
{{--                            <span class="title">Nota por semestre</span>--}}
{{--                        </a>--}}
{{--                    </li>   <li class="{{ $request->is('/reports/engagements') }}">--}}
{{--                        <a href="{{ url('/admin/reports/engagements') }}">--}}
{{--                            <i class="fa fa-line-chart"></i>--}}
{{--                            <span class="title">Comparações entre alunos</span>--}}
{{--                        </a>--}}
{{--                    </li>   <li class="{{ $request->is('/reports/reactions') }}">--}}
{{--                        <a href="{{ url('/admin/reports/reactions') }}">--}}
{{--                            <i class="fa fa-line-chart"></i>--}}
{{--                            <span class="title">Classificações aluno</span>--}}
{{--                        </a>--}}
{{--                    </li>   <li class="{{ $request->is('/reports/comments') }}">--}}
{{--                        <a href="{{ url('/admin/reports/comments') }}">--}}
{{--                            <i class="fa fa-line-chart"></i>--}}
{{--                            <span class="title">Evolução entre semestres</span>--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li class="{{ $request->is('/reports/shares') }}">--}}
{{--                        <a href="{{ url('/admin/reports/shares') }}">--}}
{{--                            <i class="fa fa-line-chart"></i>--}}
{{--                            <span class="title">Notas</span>--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                </ul>--}}
{{--            </li>--}}
{{--            @endif--}}

{{--            @if(\Auth::user()->aluno ==0)--}}

{{--            @can('engagement_access')--}}
{{--                <li class="{{ $request->segment(2) == 'engagements' ? 'active' : '' }}">--}}
{{--            <li class="treeview">--}}
{{--                <a href="#">--}}
{{--                    <i class="fa fa-line-chart"></i>--}}
{{--                    <span class="title">Indicadores Professor</span>--}}
{{--                    <span class="pull-right-container">--}}
{{--                        <i class="fa fa-angle-left pull-right"></i>--}}
{{--                    </span>--}}
{{--                </a>--}}
{{--                <ul class="treeview-menu">--}}
{{--                    <li class="{{ $request->is('/reports/fans') }}">--}}
{{--                        <a href="{{ url('/admin/reports/fans') }}">--}}
{{--                            <i class="fa fa-line-chart"></i>--}}
{{--                            <span class="title">Notas de turmas</span>--}}
{{--                        </a>--}}
{{--                    </li>   <li class="{{ $request->is('/reports/engagements') }}">--}}
{{--                        <a href="{{ url('/admin/reports/engagements') }}">--}}
{{--                            <i class="fa fa-line-chart"></i>--}}
{{--                            <span class="title">Comparar resultados</span>--}}
{{--                        </a>--}}
{{--                    </li>   <li class="{{ $request->is('/reports/reactions') }}">--}}
{{--                        <a href="{{ url('/admin/reports/reactions') }}">--}}
{{--                            <i class="fa fa-line-chart"></i>--}}
{{--                            <span class="title">Classificação alunos</span>--}}
{{--                        </a>--}}
{{--                    </li>   <li class="{{ $request->is('/reports/comments') }}">--}}
{{--                        <a href="{{ url('/admin/reports/comments') }}">--}}
{{--                            <i class="fa fa-line-chart"></i>--}}
{{--                            <span class="title">Comparativos do semestre</span>--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li class="{{ $request->is('/reports/shares') }}">--}}
{{--                        <a href="{{ url('/admin/reports/shares') }}">--}}
{{--                            <i class="fa fa-line-chart"></i>--}}
{{--                            <span class="title">Shares</span>--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                </ul>--}}
{{--            </li>--}}
{{--            @endcan--}}
{{--           @endif--}}

{{--            <li class="{{ $request->segment(1) == 'change_password' ? 'active' : '' }}">--}}
{{--                <a href="{{ route('auth.change_password') }}">--}}
{{--                    <i class="fa fa-key"></i>--}}
{{--                    <span class="title">@lang('global.app_change_password')</span>--}}
{{--                </a>--}}
{{--            </li>--}}

{{--            <li>--}}
{{--                <a href="#logout" onclick="$('#logout').submit();">--}}
{{--                    <i class="fa fa-arrow-left"></i>--}}
{{--                    <span class="title">@lang('global.app_logout')</span>--}}
{{--                </a>--}}
{{--            </li>--}}
{{--        </ul>--}}
{{--    </section>--}}
{{--</aside>--}}





<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ url('/') }}">
        <div class="sidebar-brand-icon">
{{--            <i class="fas fa-laugh-wink"></i>--}}
            <img width="100px" src="https://auth.laureate.net/adfs/portal/logo/uam.png" alt="Logo Image">
        </div>
        <div class="sidebar-brand-text mx-3">SIEA<sup>beta</sup></div>
    </a>
    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
{{--    <li class="nav-item active">  --}}
    <li class="nav-item {{ $request->segment(1) == 'home' ? 'active' : '' }}">
        <a class="nav-link" href="{{ url('/') }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Dados
    </div>

    <!-- Nav Item - Pages Collapse Menu -->
    @can('user_management_access')
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-fw fa-cog"></i>
            <span>Usuários</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Gerenciamento:</h6>

                @can('permission_access')
                <a class="collapse-item {{ $request->segment(2) == 'permissions' ? 'active active-sub' : '' }}" href="{{ route('admin.permissions.index') }}">Permissões</a>
                @endcan
                @can('role_access')
                <a class="collapse-item {{ $request->segment(2) == 'roles' ? 'active active-sub' : '' }}" href="{{ route('admin.roles.index') }}">Funções</a>
                @endcan
                @can('user_access')
                <a class="collapse-item {{ $request->segment(2) == 'users' ? 'active active-sub' : '' }}" href="{{ route('admin.users.index') }}">Usuários</a>
                @endcan

            </div>
        </div>
    </li>
    @endcan
    <!-- Nav Item - Utilities Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
            <i class="fas fa-fw fa-wrench"></i>
            <span>Perfil</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Meus Dados</h6>
                <a class="collapse-item" href="#">Meus dados</a>
                <a class="collapse-item" href="{{ route('auth.change_password') }}">Alterar Senha</a>
            </div>
        </div>
    </li>
@if(\Auth::user()->aluno ==0 || \Auth::user()->aluno ==2)
    @can('engagement_access')
    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Professor
    </div>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item {{ $request->segment(2) == 'engagements' ? 'active' : '' }}" >
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
            <i class="fas fa-fw fa-table"></i>
            <span>Indicadores Professor</span>
        </a>
        <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Teste de Progresso</h6>
                <a class="collapse-item" href="{{ url('/admin/grafics/notas-turmas') }}">Notas de turmas</a>
                <a class="collapse-item" href="{{ url('/admin/grafics/notas-turmas') }}">Comparar Resultados</a>
                <a class="collapse-item" href="{{ url('/admin/grafics/notas-turmas') }}">Classificação Alunos</a>
                <a class="collapse-item" href="{{ url('/admin/grafics/notas-turmas') }}">Comparativos Semestre</a>
                <div class="collapse-divider"></div>
                <h6 class="collapse-header">Buscar Aluno</h6>
                <a class="collapse-item" href="#">Notas</a>
                <a class="collapse-item" href="#">Desempenho</a>
            </div>
        </div>
    </li>
    @endcan
@endif

@if(\Auth::user()->aluno ==0  || \Auth::user()->aluno ==2)
    @can('engagement_access')
    <!-- Nav Item - Charts -->
    <li class="nav-item {{ $request->segment(2) == 'historics' ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('admin.historics.index') }}">
            <i class="fas fa-fw fa-folder"></i>
            <span>@lang('global.historics.title')</span></a>
    </li>
        @endcan
    @endif

@if(\Auth::user()->aluno == 1 || \Auth::user()->aluno ==2)
    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Aluno
    </div>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePagesAlunos" aria-expanded="true" aria-controls="collapsePagesAlunos">
            <i class="fas fa-fw fa-table"></i>
            <span>Indicadores Aluno</span>
        </a>
        <div id="collapsePagesAlunos" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Notas</h6>
                <a class="collapse-item" href="{{ url('/admin/grafics/notas-turmas') }}">Nota por semestre</a>
                <a class="collapse-item" href="{{ url('/admin/grafics/notas-turmas') }}">Comparações entre alunos</a>
                <a class="collapse-item" href="{{ url('/admin/grafics/notas-turmas') }}">Classificações aluno</a>
                <a class="collapse-item" href="{{ url('/admin/grafics/notas-turmas') }}">Evolução entre semestres</a>
            </div>
        </div>
    </li>
@endif
    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
<!-- End of Sidebar -->
