**#Antes de tudo certifique-se das três instações principais em sua máquina **

PHP 7.2
LARAVEL
MySQL

**#Passo a passo para clonar o projeto **

**1. Clone o repositório com git clone para sua pasta de proejetos**

git remote add origin git@gitlab.com:uam-si/siea.git

**2. Arquivo .env é o arquivo de configurações e banco de dados mysql**

Copie .env.example arquivo na raiz para .env e edite as informaçẽos de banco de dados etc.

**3. Instale o composer com o comando abaixo no terminal linux ou git bach no windows**

Run composer install

**3. Rode o comando na raíz para gerar as chaves láravel**

Run php artisan key:generate

**4. Após ter configurado as informações de BD crie um esquema/banco no mysql de** 
**acordo o que vc setou em name_db no .env, rode as migrate e seed para**
**criação das tabelas e configurações inicias do sistema**

Run php artisan migrate --seed

**5. Acesse o sistema em localhost no porta 8000:**
 php artisan serve
 [ http://127.0.0.1:800](http://127.0.0.1:800)

com as credenciais abaixo login: 
admin@admin.com / senha: password