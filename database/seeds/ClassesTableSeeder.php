<?php

use Illuminate\Database\Seeder;
use\App\Classe;

class ClassesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Classe::create(['turma' => 'CCOMP2N06A', 'campos' => 'Vila Olimpia', 'curso' => 'CIÊNCIA DA COMPUTAÇÃO']);
        Classe::create(['turma' => 'CCOMPAM02A', 'campos' => 'Vila Olimpia', 'curso' => 'CIÊNCIA DA COMPUTAÇÃO']);
        Classe::create(['turma' => 'CCOMP2N08A', 'campos' => 'Vila Olimpia', 'curso' => 'CIÊNCIA DA COMPUTAÇÃO']);
    }
}
