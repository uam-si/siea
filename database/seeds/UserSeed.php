<?php

use Illuminate\Database\Seeder;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            
            ['id' => 1, 'matricula'=>'11111111', 'name' => 'Admin', 'email' => 'admin@admin.com', 'password' => '$2y$10$ej0wwB/GmNHAaGGpzD5ACO1E3MyoeRHjy0geg/qQnZxqVrH/BoC6u', 'aluno' => 0, 'active'=>1, 'remember_token' => '',],
            ['id' => 2, 'matricula'=>'00000001', 'name' => 'Roberta Aragon', 'email' => 'professor@gmail.com', 'password' => '$2y$10$ej0wwB/GmNHAaGGpzD5ACO1E3MyoeRHjy0geg/qQnZxqVrH/BoC6u', 'aluno' => 0, 'active'=>1, 'remember_token' => '',],
            ['id' => 3, 'matricula'=>'21051386', 'name' => 'Wesley Souza', 'email' => 'wesley.santos@gmail.com', 'password' => '$2y$10$ej0wwB/GmNHAaGGpzD5ACO1E3MyoeRHjy0geg/qQnZxqVrH/BoC6u', 'aluno' => 0, 'active'=>1, 'remember_token' => '',],

        ];

        foreach ($items as $item) {
            \App\User::create($item);
        }
    }
}
