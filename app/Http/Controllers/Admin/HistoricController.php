<?php

namespace App\Http\Controllers\Admin;

use App\Historic;
use App\Http\Requests\Admin\UpdatesRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;

class HistoricController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Gate::allows('engagement_access')) {
            return abort(401);
        }

        if (request('show_deleted') == 1) {
            if (! Gate::allows('engagement_delete')) {
                return abort(401);
            }
            $historics = Historic::onlyTrashed()->get();
        } else {
            $historics = Historic::all();
        }



        return view('admin.historics.index', compact('historics'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('engagement_create')) {
            return abort(401);
        }

        return view('admin.historics.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        dd($request->toArray());
        if (! Gate::allows('engagement_create')) {
            return abort(401);
        }

        $historic = Historic::create($request->all());

        return redirect()->route('admin.historics.index');
    }


    public function edit($id)
    {
        if (! Gate::allows('engagement_edit')) {
            return abort(401);
        }
        $historic = Historic::findOrFail($id);

        return view('admin.historics.edit', compact('historic'));
    }

    /**
     * Update Engagement in storage.
     *
     * @param  \App\Http\Requests\UpdateEngagementsRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateHistoricsRequest $request, $id)
    {
        if (! Gate::allows('engagement_edit')) {
            return abort(401);
        }
        $$historic = Historic::findOrFail($id);
        $historic->update($request->all());

        return redirect()->route('admin.historics.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Historic  $historic
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('engagement_view')) {
            return abort(401);
        }
        $historic = Historic::findOrFail($id);

        return view('admin.historics.show', compact('historic'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Historic  $historic
     * @return \Illuminate\Http\Response
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('engagement_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Historic::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

    /**
     * Restore Engagement from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        if (! Gate::allows('engagement_delete')) {
            return abort(401);
        }
        $historic = Historic::onlyTrashed()->findOrFail($id);
        $historic->restore();

        return redirect()->route('admin.historics.index');
    }

    /**
     * Permanently delete Engagement from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function perma_del($id)
    {
        if (! Gate::allows('engagement_delete')) {
            return abort(401);
        }
        $historic = Historic::onlyTrashed()->findOrFail($id);
        $historic->forceDelete();

        return redirect()->route('admin.historics.index');
    }

}
