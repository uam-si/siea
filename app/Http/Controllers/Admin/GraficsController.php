<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Historic;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class GraficsController extends Controller
{

    public function index(Historic $historic){
        $graficTitle = 'Notas de turmas';
        $graficLabel = 'SUM';
        $chartType   = 'line';

//        $ClassesTeacher = DB::table('users')
//            ->join('user_classes', 'users.id', '=', 'user_classes.user_id')
//            ->join('classes', 'classes.id', '=', 'user_classes.classe_id')
//            ->join('historics', 'historics.turma', '=', 'classes.turma')
//            ->select('users.id', 'users.name', 'classes.turma', 'classes.campos', 'classes.curso', 'historics.nome', 'historics.class_geral')
//            ->where('users.id', Auth::user()->id)
//            ->get();

        $query = 'SELECT DISTINCT classes.turma, users.name, historics.*
              FROM classes
              JOIN user_classes ON classes.id = user_classes.classe_id
              JOIN users ON user_classes.user_id = users.id
              JOIN historics ON classes.turma=historics.turma
              WHERE classes.turma IN (SELECT distinct turma FROM historics)
              AND users.id=' . Auth::user()->id;

        $query .= ' order by historics.turno ASC';

        $return = DB::select($query);
        dd($return);

       // return view('admin.grafics', compact('graficTitle','teste', 'chartType', 'graficLabel'));
    }

    public function notasTurmas(){
        $graficTitle = 'Notas Por Turma';
        $graficLabel = 'Lista de notas por Turma';
        $graficId   = 'notasTurmas';
        $chartType   = 'line';

        $query = 'SELECT DISTINCT classes.turma, users.name, historics.nome, historics.turno, 
                  historics.gabarito, historics.nota_do_teste, historics.total_de_acertos   
              FROM classes
              JOIN user_classes ON classes.id = user_classes.classe_id
              JOIN users ON user_classes.user_id = users.id
              JOIN historics ON classes.turma=historics.turma
              WHERE classes.turma IN ("CCOMP2N08A")
              AND users.id=' . Auth::user()->id;
        // SELECT distinct turma FROM historics
        $query .= ' order by historics.turno ASC';
        $grafics = [];
        $grafics = DB::select($query);
        //dd($grafics);
        return view('admin.graficsteacher', compact('graficTitle', 'grafics', 'chartType', 'graficLabel', 'graficId'));
    }

/*=================================================*/
    public function classTurma()
    {
        $graficTitle = 'Classificação Turma';
        $graficLabel = 'Posição Classificação Aluno Turma';
        $graficId   = 'classTurma';
        $chartType   = 'line';

        $query = DB::table('historics')
            ->select('turno', 'class_turma')
            ->where('matricula', Auth::user()->matricula)
            ->orderBy('turno');

        $queries = $query->addSelect('turma', 'turno')->get();

        $grafics = [];
        foreach ($queries as $grafic) {
            $grafics[$grafic->turno] = $grafic->class_turma;
        }
        $grafics2 = [];
        foreach ($queries as $grafic) {
            $grafics2[$grafic->turno] = $grafic->turma;
        }

        return view('admin.grafics', compact('graficTitle', 'grafics', 'grafics2', 'chartType', 'graficLabel', 'graficId'));
    }

    public function classUnidade()
    {
        $graficTitle = 'Classificação Unidade';
        $graficLabel = 'Classificação Por Unidade do Estudante';
        $graficId   = 'classUnidade';
        $chartType   = 'line';

        $queries = DB::table('historics')
            ->select('turno', 'class_unidade')
            ->where('matricula', Auth::user()->matricula)
            ->orderBy('turno');

        $grafics = [];
        foreach ($queries as $grafic) {
            $grafics[$grafic->turno] = $grafic->class_unidade;
        }

        return view('admin.grafics', compact('graficTitle', 'grafics', 'chartType', 'graficLabel', 'graficId'));
    }

    public function classGeral()
    {
        $graficTitle = 'Classificação Geral';
        $graficLabel = 'Classificação Geral do Curso';
        $graficId   = 'classGeral';
        $chartType   = 'line';

        $queries = DB::table('historics')
            ->select('turno', 'class_geral')
            ->where('matricula', Auth::user()->matricula)
            ->orderBy('turno');

        $grafics = [];
        foreach ($queries as $grafic) {
            $grafics[$grafic->turno] = $grafic->class_geral;
        }

        return view('admin.grafics', compact('graficTitle', 'grafics', 'chartType', 'graficLabel', 'graficId'));
    }

    public function notasAluno()
    {
        $graficTitle = 'Notas aluno';
        $graficLabel = 'Notas do Semestres e Tipo de Provas';
        $graficId   = 'notasAluno';
        $chartType   = 'line';
        /*turno, gabarito, nota_do_teste, total_de_acertos*/
        $queries = DB::table('historics')
            ->select('turno', 'gabarito', 'nota_do_teste', 'total_de_acertos')
            ->where('matricula', Auth::user()->matricula)
            ->orderBy('turno');

        $grafics = [];
        foreach ($queries as $grafic) {
            $grafics[$grafic->turno] = $grafic->nota_do_teste;
            $grafics[$grafic->gabarito] = $grafic->total_de_acertos;
        }

        return view('admin.grafics', compact('graficTitle', 'grafics', 'chartType', 'graficLabel', 'graficId'));
    }

    public function questoesGerais()
    {
        $graficTitle = 'Conhecimentos Gerais';
        $graficLabel = 'Acertos Questões Conhecimentos Gerais';
        $graficId   = 'questoesGerais';
        $chartType   = 'line';
        /*turno, gabarito, fundamentacao_geral, acertos_fundamentacao_geral*/
        $queries = DB::table('historics')
            ->select('turno', 'gabarito', 'fundamentacao_geral', 'acertos_fundamentacao_geral')
            ->where('matricula', Auth::user()->matricula)
            ->orderBy('turno');

        $grafics = [];
        foreach ($queries as $grafic) {
            $grafics[$grafic->turno] = $grafic->fundamentacao_geral;
            $grafics[$grafic->gabarito] = $grafic->acertos_fundamentacao_geral;
        }

        return view('admin.grafics', compact('graficTitle', 'grafics', 'chartType', 'graficLabel', 'graficId'));
    }

    public function questoesEspecificas()
    {
        $graficTitle = 'Conhecimentos Específicos';
        $graficLabel = 'Acertos Questões Conhecimentos Específicos';
        $graficId   = 'questoesEspecificas';
        $chartType   = 'line';
        /*turno, gabarito, conhecimentos_especificos, acertos_conhecimentos_especificos*/
        $queries = DB::table('historics')
            ->select('turno', 'gabarito', 'conhecimentos_especificos', 'acertos_conhecimentos_especificos')
            ->where('matricula', Auth::user()->matricula)
            ->orderBy('turno');

        $grafics = [];
        foreach ($queries as $grafic) {
            $grafics[$grafic->turno] = $grafic->conhecimentos_especificos;
            $grafics[$grafic->gabarito] = $grafic->acertos_conhecimentos_especificos;
        }

        return view('admin.grafics', compact('graficTitle', 'grafics', 'chartType', 'graficLabel', 'graficId'));
    }

    public function acertosPorcentagemAluno()
    {
        $graficTitle = 'Porcentagem de acertos';
        $graficLabel = 'Porcentagem Acertos Questões Gerais, Específicos e Total Acertos';
        $graficId   = 'acertosPorcentagemAluno';
        $chartType   = 'line';
        /*turno, gabarito, porcentagem_conhecimentos_especificos, porcentagem_conhecimentos_geral, total_porcentagem, stats_date*/
        $queries = DB::table('historics')
            ->select('turno', 'gabarito', 'porcentagem_conhecimentos_especificos', 'porcentagem_conhecimentos_geral', 'total_porcentagem', 'stats_date')
            ->where('matricula', Auth::user()->matricula)
            ->orderBy('turno');

        $grafics = [];
        foreach ($queries as $grafic) {
            $grafics[$grafic->turno] = $grafic->porcentagem_conhecimentos_especificos;
            $grafics[$grafic->gabarito] = $grafic->porcentagem_conhecimentos_geral;
            $grafics[$grafic->stats_date] = $grafic->total_porcentagem;
        }

        return view('admin.grafics', compact('graficTitle', 'grafics', 'chartType', 'graficLabel', 'graficId'));
    }

    public function engagements()
    {
        $graficTitle = 'Comparar resultados';
        $graficLabel = 'SUM';
        $chartType   = 'bar';

        $grafics = Historic::get()->sortBy('stats_date')->groupBy(function ($entry) {

            if ($entry->stats_date instanceof \Carbon\Carbon) {
                return \Carbon\Carbon::parse($entry->stats_date)->format('Y-m-d');
            }

            return \Carbon\Carbon::createFromFormat(config('app.date_format'), $entry->stats_date)->format('Y-m-d');
        })->map(function ($entries, $group) {
            return $entries->sum('engagements');
        });

        return view('admin.grafics', compact('graficTitle', 'grafics', 'chartType', 'graficLabel'));
    }

    public function reactions()
    {
        $graficTitle = 'Classificação alunos';
        $graficLabel = 'SUM';
        $chartType   = 'line';

        $grafics = Historic::get()->sortBy('stats_date')->groupBy(function ($entry) {
            if ($entry->stats_date instanceof \Carbon\Carbon) {
                return \Carbon\Carbon::parse($entry->stats_date)->format('Y-m-d');
            }

            return \Carbon\Carbon::createFromFormat(config('app.date_format'), $entry->stats_date)->format('Y-m-d');
        })->map(function ($entries, $group) {
            return $entries->sum('reactions');
        });

        return view('admin.grafics', compact('graficTitle', 'grafics', 'chartType', 'graficLabel'));
    }

    public function comments()
    {
        $graficTitle = 'Comparativos do semestre';
        $graficLabel = 'SUM';
        $chartType   = 'line';

        $grafics = Historic::get()->sortBy('stats_date')->groupBy(function ($entry) {
            if ($entry->stats_date instanceof \Carbon\Carbon) {
                return \Carbon\Carbon::parse($entry->stats_date)->format('Y-m-d');
            }

            return \Carbon\Carbon::createFromFormat(config('app.date_format'), $entry->stats_date)->format('Y-m-d');
        })->map(function ($entries, $group) {
            return $entries->sum('comments');
        });

        return view('admin.grafics', compact('graficTitle', 'grafics', 'chartType', 'graficLabel'));
    }

    public function shares()
    {
        $graficTitle = 'Shares';
        $graficLabel = 'SUM';
        $chartType   = 'line';

        $grafics = Historic::get()->sortBy('stats_date')->groupBy(function ($entry) {
            if ($entry->stats_date instanceof \Carbon\Carbon) {
                return \Carbon\Carbon::parse($entry->stats_date)->format('Y-m-d');
            }

            return \Carbon\Carbon::createFromFormat(config('app.date_format'), $entry->stats_date)->format('Y-m-d');
        })->map(function ($entries, $group) {
            return $entries->sum('shares');
        });

        return view('admin.grafics', compact('graficTitle', 'grafics', 'chartType', 'graficLabel'));
    }

}
