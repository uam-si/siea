<!DOCTYPE html>
<html lang="en">

<head>
    @include('partials.head')
</head>


{{--<body class="hold-transition skin-blue sidebar-mini">--}}

{{--<div id="wrapper">--}}

<body id="page-top">

<!-- Page Wrapper -->
<div id="wrapper">

@include('partials.sidebar')
@include('partials.topbar')

{{--<!-- Content Wrapper. Contains page content -->--}}
{{--    <div class="content-wrapper">--}}
{{--        <!-- Main content -->--}}



{{--        <section class="content">--}}

{{--            @if(isset($siteTitle))--}}
{{--                <h3 class="page-title">--}}
{{--                    {{ $siteTitle }}--}}
{{--                </h3>--}}
{{--            @endif--}}

{{--            <div class="row">--}}
{{--                <div class="col-md-12">--}}

{{--                    @if (Session::has('message'))--}}
{{--                        <div class="alert alert-info">--}}
{{--                            <p>{{ Session::get('message') }}</p>--}}
{{--                        </div>--}}
{{--                    @endif--}}
{{--                    @if ($errors->count() > 0)--}}
{{--                        <div class="alert alert-danger">--}}
{{--                            <ul class="list-unstyled">--}}
{{--                                @foreach($errors->all() as $error)--}}
{{--                                    <li>{{ $error }}</li>--}}
{{--                                @endforeach--}}
{{--                            </ul>--}}
{{--                        </div>--}}
{{--                    @endif--}}

<!-- Begin Page Content -->
    <div class="container-fluid">

                    @yield('content')


</div>
<!-- End of Main Content -->

{{--                </div>--}}
{{--            </div>--}}
{{--        </section>--}}
{{--    </div>--}}
{{--</div>--}}

{!! Form::open(['route' => 'auth.logout', 'style' => 'display:none;', 'id' => 'logout']) !!}
<button type="submit">Logout</button>
{!! Form::close() !!}


<!-- Footer -->
    <footer class="sticky-footer bg-white">
        <div class="container my-auto">
            <div class="copyright text-center my-auto">
                <span>SIEA &copy; Universidade Anhembi Morumbi 2019</span>
            </div>
        </div>
    </footer>
    <!-- End of Footer -->

</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" href="login.html">Logout</a>
            </div>
        </div>
    </div>
</div>
@include('partials.javascripts')
</body>
</html>