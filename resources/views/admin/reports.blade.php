@extends('layouts.app')

@section('content')
    <div class="row">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
{{--        @foreach ($reports as $report)--}}
        <div class="col-md-6">
            <h2 style="margin-top: 0;">{{ $reportTitle }}</h2>

            <canvas id="myChart{{ $reportTitle }}"></canvas>

            <script>
                var ctx = document.getElementById("myChart{{ $reportTitle }}");
                var myChart = new Chart(ctx, {
                    type: '{{ $chartType }}',
                    data: {
                        labels: [
                            @foreach ($reports as $group => $result)
                                "{{ $group }}",
                            @endforeach
                        ],

                        datasets: [{
                            label: '{{ $reportLabel }}',
                            data: [
                                @foreach ($reports as $group => $result)
                                    {!! $result !!},
                                @endforeach
                            ],
                            borderWidth: 1
                        }]
                    },
                    options: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero:true
                                }
                            }]
                        }
                    }
                });
            </script>
        </div>
{{--        @endforeach--}}
    </div>
@stop
